# Embedd-S7com

v1.0

## 介绍
本库由 slickzz 负责，作为底层 S7 通信的仓库

## 开发环境
编辑器：vscode

编译器：arm-gnu-toolchain-12.3.rel1-mingw-w64-i686-arm-none-eabi

构建工具：make

## 项目流程

[ok]1. 移植 S7Helper 

[ok]2. 完成 w5500 NIC 在 STM32 上的移植

[ok]3. 熟悉 S7 通信协议

[ok]4. 移植 S7Client

5. 测试验收

## 开源库的链接

适合在单片机上使用的 S7 协议栈：
https://settimino.sourceforge.net/

w5500 NIC 驱动：
https://github.com/Wiznet/ioLibrary_Driver
