#include "ucS7.h"

#define registerfuntion(type, funtion, name, ...) type funtion##name(__VA_ARGS__)

#include <string.h>

const byte S7CpuStatusUnknown = 0x00;
const byte S7CpuStatusRun     = 0x08;
const byte S7CpuStatusStop    = 0x04;

// ISO Connection Request telegram (contains also ISO Header and COTP Header)
	byte ISO_CR[] = {
		// TPKT (RFC1006 Header)
		0x03, // RFC 1006 ID (3) 
		0x00, // Reserved, always 0
		0x00, // High part of packet lenght (entire frame, payload and TPDU included)
		0x16, // Low part of packet lenght (entire frame, payload and TPDU included)
		// COTP (ISO 8073 Header)
		0x11, // PDU Size Length
		0xE0, // CR - Connection Request ID
		0x00, // Dst Reference HI
		0x00, // Dst Reference LO
        0x00, // Src Reference HI
		0x01, // Src Reference LO
		0x00, // Class + Options Flags
		0xC0, // PDU Max Length ID
		0x01, // PDU Max Length HI
		0x0A, // PDU Max Length LO
		0xC1, // Src TSAP Identifier
		0x02, // Src TSAP Length (2 bytes)
        0x01, // Src TSAP HI (will be overwritten by ISOConnect())
		0x00, // Src TSAP LO (will be overwritten by ISOConnect())
		0xC2, // Dst TSAP Identifier
		0x02, // Dst TSAP Length (2 bytes)
		0x01, // Dst TSAP HI (will be overwritten by ISOConnect())
		0x02, // Dst TSAP LO (will be overwritten by ISOConnect())
	};

// S7 PDU Negotiation Telegram (contains also ISO Header and COTP Header)
	byte S7_PN[] = {
		0x03, 0x00, 0x00, 0x19, 0x02, 0xf0, 0x80, // TPKT + COTP (see above for info)
		0x32, 0x01, 0x00, 0x00, 0x04, 0x00, 0x00, 0x08, 0x00, 
		0x00, 0xf0, 0x00, 0x00, 0x01, 0x00, 0x01, 
		0x00, 0xf0 // PDU Length Requested = HI-LO 240 bytes
	};

// S7 Read/Write Request Header (contains also ISO Header and COTP Header)
	byte S7_RW[] = { // 31-35 bytes
		0x03, 0x00, 
		0x00, 0x1f, // Telegram Length (Data Size + 31 or 35)
		0x02, 0xf0, 0x80, // COTP (see above for info)
		0x32,       // S7 Protocol ID 
		0x01,       // Job Type
		0x00, 0x00, // Redundancy identification
		0x05, 0x00, // PDU Reference
		0x00, 0x0e, // Parameters Length
		0x00, 0x00, // Data Length = Size(bytes) + 4      
		0x04,       // Function 4 Read Var, 5 Write Var  
		0x01,       // Items count
		0x12,       // Var spec.
		0x0a,       // Length of remaining bytes
		0x10,       // Syntax ID 
		S7WLByte,   // Transport Size (default, could be changed)                       
		0x00,0x00,  // Num Elements                          
		0x00,0x00,  // DB Number (if any, else 0)            
		0x84,       // Area Type                            
		0x00, 0x00, 0x00, // Area Offset                     
		// WR area
		0x00,       // Reserved 
		0x04,       // Transport size
		0x00, 0x00, // Data Length * 8 (if not timer or counter) 
	};

#ifdef _EXTENDED

// S7 Get Block Info Request Header (contains also ISO Header and COTP Header)
	byte S7_BI[] = {
		0x03, 0x00, 0x00, 0x25, 0x02, 0xf0, 0x80, 0x32, 
		0x07, 0x00, 0x00, 0x05, 0x00, 0x00, 0x08, 0x00, 
		0x0c, 0x00, 0x01, 0x12, 0x04, 0x11, 0x43, 0x03, 
		0x00, 0xff, 0x09, 0x00, 0x08, 0x30, 0x41, 
		0x30, 0x30, 0x30, 0x30, 0x30, // ASCII DB Number
		0x41 
	};

// S7 Put PLC in STOP state Request Header (contains also ISO Header and COTP Header)
	byte S7_STOP[] = {
		0x03, 0x00, 0x00, 0x21, 0x02, 0xf0, 0x80, 0x32, 
		0x01, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x10, 0x00, 
		0x00, 0x29, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 
		0x50, 0x5f, 0x50, 0x52, 0x4f, 0x47, 0x52, 0x41, 
		0x4d 
	};

// S7 Put PLC in RUN state Request Header (contains also ISO Header and COTP Header)
	byte S7_START[] = {
		0x03, 0x00, 0x00, 0x25, 0x02, 0xf0, 0x80, 0x32, 
		0x01, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x14, 0x00, 
		0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0xfd, 0x00, 0x00, 0x09, 0x50, 0x5f, 0x50, 0x52, 
		0x4f, 0x47, 0x52, 0x41, 0x4d 
	};

// S7 Get PLC Status Request Header (contains also ISO Header and COTP Header)
	byte S7_PLCGETS[] = {
		0x03, 0x00, 0x00, 0x21, 0x02, 0xf0, 0x80, 0x32, 
		0x07, 0x00, 0x00, 0x2c, 0x00, 0x00, 0x08, 0x00, 
		0x08, 0x00, 0x01, 0x12, 0x04, 0x11, 0x44, 0x01, 
		0x00, 0xff, 0x09, 0x00, 0x04, 0x04, 0x24, 0x00, 
		0x00 
	};

#endif // _EXTENDED

	TPDU PDU;

#ifdef _ucS7HELPER

registerfuntion(Status, BitAt, 1, void *Buffer, int ByteIndex, byte BitIndex)
{
	byte mask[] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
	pbyte Pointer = (pbyte)(Buffer) + ByteIndex;
	
	if (BitIndex>7)
		return Error;
	else
		return (*Pointer & mask[BitIndex]);	
}

registerfuntion(Status, BitAt, 2,int ByteIndex, int BitIndex)
{
	return BitAt1(&PDU.DATA[0], ByteIndex, BitIndex);
}

registerfuntion(byte, ByteAt, 1, void *Buffer, int index)
{
	pbyte Pointer = (pbyte)(Buffer) + index;
	return *Pointer;
}

registerfuntion(byte, ByteAt, 2, int index)
{
	return ByteAt1(&PDU.DATA, index);
}

registerfuntion(word, WordAt, 1, void *Buffer, int index)
{
	word hi=(*((pbyte)(Buffer) + index))<<8;
	return hi+*((pbyte)(Buffer) + index+1);	
}

registerfuntion(word, WordAt, 2, int index)
{
	return WordAt1(&PDU.DATA, index);
}

registerfuntion(dword, DWordAt, 1, void *Buffer, int index)
{
	pbyte pb;
	dword dw1;

	pb=(pbyte)(Buffer) + index;
	dw1=*pb;dw1<<=8;
	pb=(pbyte)(Buffer) + index + 1;
	dw1+=*pb;dw1<<=8;
	pb=(pbyte)(Buffer) + index + 2;
	dw1+=*pb;dw1<<=8;
	pb=(pbyte)(Buffer) + index + 3;
	dw1+=*pb;
	return dw1;	
}

registerfuntion(dword, DWordAt, 2, int index)
{
	return DWordAt1(&PDU.DATA, index);
}

registerfuntion(float, FloatAt, 1, void *Buffer, int index)
{
	dword dw = DWordAt1(Buffer, index);
	return *((pfloat)(&dw));	
}

registerfuntion(float, FloatAt, 2, int index)
{
	return FloatAt1(&PDU.DATA, index);	
}

registerfuntion(integer, IntegerAt, 1, void *Buffer, int index)
{
	word w = WordAt1(Buffer, index);
	return *((pinteger)(&w));
}

registerfuntion(integer, IntegerAt, 2, int index)
{
	return IntegerAt1(&PDU.DATA, index);
}

registerfuntion(long, DintAt, 1, void *Buffer, int index)
{
	dword dw = DWordAt1(Buffer, index);
	return *((pdint)(&dw));	
}

registerfuntion(long, DintAt, 2, int index)
{
	return DintAt1(&PDU.DATA, index);	
}

registerfuntion(void , SetBitAt, 1, void *Buffer, int ByteIndex, int BitIndex, bit Value)
{
	byte Mask[] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
	pbyte Pointer = (pbyte)(Buffer) + ByteIndex;
	if (BitIndex < 0) BitIndex = 0;
	if (BitIndex > 7) BitIndex = 7;

	if (Value)
		*Pointer=*Pointer | Mask[BitIndex];
	else
		*Pointer=*Pointer & ~Mask[BitIndex];	
} 

registerfuntion(void , SetBitAt, 2, int ByteIndex, int BitIndex, bit Value)
{
	SetBitAt1(&PDU.DATA, ByteIndex, BitIndex, Value);	
} 

registerfuntion(void, SetByteAt, 1, void *Buffer, int index, byte value)
{
	*((pbyte)(Buffer)+index)=value;
}

registerfuntion(void, SetByteAt, 2, int index, byte value)
{
	PDU.DATA[index]=value;
}

registerfuntion(void, SetIntAt, 1, void *Buffer, int index, integer value)
{
	*((pbyte)(Buffer)+index)  = (byte)(value>>8);
	*((pbyte)(Buffer)+index+1)= (byte)(value & 0x00FF);	
}

registerfuntion(void, SetIntAt, 2, int index, integer value)
{
	SetIntAt1(&PDU.DATA, index, value);
}

registerfuntion(void, SetDIntAt, 1, void *Buffer, int index, dint value)
{
	*((pbyte)(Buffer)+index)  = (byte)((value >> 24) & 0xFF);
	*((pbyte)(Buffer)+index+1)= (byte)((value >> 16) & 0xFF);
	*((pbyte)(Buffer)+index+2)= (byte)((value >> 8) & 0xFF);
	*((pbyte)(Buffer)+index+3)= (byte)(value & 0x00FF);	
}

registerfuntion(void, SetDIntAt, 2, int index, dint value)
{
	SetDIntAt1(&PDU.DATA, index, value);
}

registerfuntion(void, SetWordAt, 1, void *Buffer, int index, word value)
{
	*((pbyte)(Buffer)+index)  = (byte)(value>>8);
	*((pbyte)(Buffer)+index+1)= (byte)(value & 0x00FF);	
}

registerfuntion(void, SetWordAt, 2, int index, word value)
{
   SetWordAt1(&PDU.DATA, index, value);	
}

registerfuntion(void , SetDWordAt, 1, void *Buffer, int index, dword value)
{
	*((pbyte)(Buffer)+index)  = (byte)((value >> 24) & 0xFF);
	*((pbyte)(Buffer)+index+1)= (byte)((value >> 16) & 0xFF);
	*((pbyte)(Buffer)+index+2)= (byte)((value >> 8) & 0xFF);
	*((pbyte)(Buffer)+index+3)= (byte)(value & 0x00FF);	
}

registerfuntion(void , SetDWordAt, 2, int index, word value)
{
	SetDWordAt1(&PDU.DATA, index, value);
}

registerfuntion(void, SetFloatAt, 1, void *Buffer, int index, float value)
{
	pdword dvalue = (pdword)(&value);
  
	*((pbyte)(Buffer)+index)  = (byte)((*dvalue >> 24) & 0xFF);
	*((pbyte)(Buffer)+index+1)= (byte)((*dvalue >> 16) & 0xFF);
	*((pbyte)(Buffer)+index+2)= (byte)((*dvalue >> 8) & 0xFF);
	*((pbyte)(Buffer)+index+3)= (byte)(*dvalue & 0x00FF);	
}

registerfuntion(void, SetFloatAt, 2, int index, float value)
{
	SetFloatAt1(&PDU.DATA, index, value);
}

registerfuntion(char *, StringAt, 1, void *Buffer, int index)
{
	return (pchar)(Buffer+index);
}

registerfuntion(char *, StringAt, 2, int index)
{
	return (pchar)(&PDU.DATA[index]);
}

registerfuntion(void, SetStringAt, 1, void *Buffer, int index, char *value)
{
	strcpy((pchar)(Buffer+index),value);
}

registerfuntion(void, SetStringAt, 2, int index, char *value)
{
	strcpy((pchar)(&PDU.DATA[index]),value);
}

ucS7Helper ucS7 = {
	.BitAt1 = BitAt1,
	.BitAt2 = BitAt2,
	.ByteAt1 = ByteAt1,
	.ByteAt2 = ByteAt2,
	.WordAt1 = WordAt1,
	.WordAt2 = WordAt2,
	.DWordAt1 = DWordAt1,
	.DWordAt2 = DWordAt2,
	.FloatAt1 = FloatAt1,
	.FloatAt2 = FloatAt2,
	.IntegerAt1 = IntegerAt1,
	.IntegerAt2 = IntegerAt2,
	.DintAt1 = DintAt1,
	.DintAt2 = DintAt2,
	.SetBitAt1 = SetBitAt1,
	.SetBitAt2 = SetBitAt2,
	.SetByteAt1 = SetByteAt1,
	.SetByteAt2 = SetByteAt2,
	.SetIntAt1 = SetIntAt1,
	.SetIntAt2 = SetIntAt2,
	.SetDIntAt1 = SetDIntAt1,
	.SetDIntAt2 = SetDIntAt2,
	.SetWordAt1 = SetWordAt1,
	.SetWordAt2 = SetWordAt2,
	.SetDWordAt1 = SetDWordAt1,
	.SetDWordAt2 = SetDWordAt2,
	.SetFloatAt1 = SetFloatAt1,
	.SetFloatAt2 = SetFloatAt2,
	.StringAt1 = StringAt1,
	.StringAt2 = StringAt2,
	.SetStringAt1 = SetStringAt1,
	.SetStringAt2 = SetStringAt2,
};
#endif // _ucS7HELPER

static uint8_t LocalTSAP_HI = 0x01; 
static uint8_t LocalTSAP_LO = 0x00;
static uint8_t RemoteTSAP_HI= 0x01;
static uint8_t RemoteTSAP_LO= 0x04;
static uint8_t LastPDUType;
static uint16_t ConnType = PG;
static IPAddress Peer[4];



static int PDULength;    // PDU Length negotiated

static int IsoPduSize()
{
    uint16_t Size = PDU.H[2];
	return (Size<<8) + PDU.H[3];
}

static int SetLastError(int Error)
{
	LastError=Error;
	return Error;
}

static int WaitForData(uint8_t sn, uint16_t Size, uint16_t Timeout)
{
    uint16_t Elapsed = HAL_GetTick();
    uint16_t BytesReady;

    do {
        BytesReady = TCPClient.tcpavailable(sn);
        if(BytesReady<Size)
        {
            HAL_Delay(50);
        } else {  
            return SetLastError(0);
        }

        if (HAL_GetTick()<Elapsed)
        {
            Elapsed=HAL_GetTick(); 
        }

    }while (HAL_GetTick() - Elapsed < Timeout);

    if (BytesReady>0)
        TCPClient.tcpflush(sn);
    else
    {
        if (!TCPClient.tcpconnected(sn))
            return SetLastError(errTCPConnectionReset);
    }

    return SetLastError(errTCPDataRecvTout);
}

static int RecvPacket(uint8_t sn, uint8_t *buf, uint16_t Size)
{
	WaitForData(sn, Size, RecvTimeout);
	if (LastError!=0)
		return LastError;
	if (TCPClient.tcpread(sn, buf, Size)==0)
		return SetLastError(errTCPConnectionReset);
	return SetLastError(0);
}

static int RecvISOPacket(uint8_t sn, uint16_t *Size)
{
	bit Done = False;
	pbyte Target = (pbyte)(&PDU.H[0])+Shift;
	LastError=0;

	while ((LastError==0) && !Done)
	{
		// Get TPKT (4 bytes)
		RecvPacket(sn, PDU.H, 4); 
		if (LastError==0)
		{
			*Size = IsoPduSize();
			// Check 0 bytes Data Packet (only TPKT+COTP - 7 bytes)
			if (*Size==7)
				RecvPacket(sn, PDU.H, 3); // Skip remaining 3 bytes and Done is still false
			else
			{
				if ((*Size>MaxPduSize) || (*Size<MinPduSize))
					LastError=errISOInvalidPDU;
				else
					Done = True; // a valid Length !=7 && >16 && <247
			}
		}
	}
	if (LastError==0)
	{
		RecvPacket(sn, PDU.H, 3); // Skip remaining 3 COTP bytes
		LastPDUType=PDU.H[1]; // Stores PDU Type, we need it 
		*Size-=ISOSize;
		// We need to align with PDU.DATA
		RecvPacket(sn, Target, *Size);
	}
	if (LastError!=0)
		TCPClient.tcpflush(sn);
	return LastError;
}

static int TCPConnect(uint8_t sn)
{
	if (TCPClient.tcpconnect(sn, Peer, isotcp))
		return SetLastError(0);
	else
		return SetLastError(errTCPConnectionFailed);
}

static int ISOConnect(uint8_t sn)
{
	// bit Done = False;
	uint16_t Length;
	// Setup TSAPs
	ISO_CR[16]=LocalTSAP_HI;
	ISO_CR[17]=LocalTSAP_LO;
	ISO_CR[20]=RemoteTSAP_HI;
	ISO_CR[21]=RemoteTSAP_LO;

	if (TCPClient.tcpwrite(sn, ISO_CR, sizeof(ISO_CR)) == sizeof(ISO_CR))
	{
		RecvISOPacket(sn, &Length);        
		if ((LastError==0) && (Length==15)) // 15 = 22 (sizeof CC telegram) - 7 (sizeof Header)
		{
			if (LastPDUType==CC) // Connection confirm
				return 0;
			else
				return SetLastError(errISOInvalidPDU);
		}
		else
			return LastError;
	}
	else
		return SetLastError(errISOInvalidPDU);
}

static int NegotiatePduLength(uint8_t sn)
{
	uint16_t Length;
	if (TCPClient.tcpwrite(sn, &S7_PN[0], sizeof(S7_PN))==sizeof(S7_PN))
	{
		RecvISOPacket(sn, &Length);
		if (LastError==0)                 
		{
			// check S7 Error
			if ((Length==20) && (PDU.H[27]==0) && (PDU.H[28]==0))  // 20 = size of Negotiate Answer
			{
				PDULength = PDU.RAW[35];
				PDULength = (PDULength<<8) + PDU.RAW[36]; // Value negotiated
				if (PDULength>0)
				    return 0;
				else
					return SetLastError(errISONegotiatingPDU);
			}
			else 
				return SetLastError(errISONegotiatingPDU);
		}
		else
			return LastError;
	}
	else
		return SetLastError(errISONegotiatingPDU);
}

bit Connected = False;
int LastError = 0;
uint16_t RecvTimeout = 500; // 500 ms

registerfuntion(void, SetConnectionParams, , IPAddress *Address, uint16_t LocalTSAP, uint16_t RemoteTSAP)
{
    memcpy(Peer,Address,4);
	LocalTSAP_HI = LocalTSAP>>8;
	LocalTSAP_LO = LocalTSAP & 0x00FF;
	RemoteTSAP_HI = RemoteTSAP>>8;
	RemoteTSAP_LO = RemoteTSAP & 0x00FF;
}

registerfuntion(void, SetConnectionType, , uint16_t ConnectionType)
{
    ConnType = ConnectionType;
}

registerfuntion(int, Connect, , uint8_t sn)
{
    LastError = 0;
	if (!Connected)
	{
		TCPConnect(sn);
		if (LastError==0) // First stage : TCP Connection
		{
            
			ISOConnect(sn);
			if (LastError==0) // Second stage : ISOTCP (ISO 8073) Connection
			{
				LastError=NegotiatePduLength(sn); // Third stage : S7 PDU negotiation
			}
		}	
	}
	Connected=LastError==0;
	return LastError;
}

registerfuntion(int, ConnectTo, , uint8_t sn, IPAddress *Address, uint16_t Rack, uint16_t Slot)
{
	SetConnectionParams(Address, 0x0100, (ConnType<<8)+(Rack * 0x20) + Slot);
	return Connect(sn);
}

registerfuntion(void, Disconnect, , uint8_t sn)
{
	if (Connected)
	{
		TCPClient.tcpstop(sn);
		Connected = False;
		PDULength = 0;
		LastError = 0;
	}	
}

registerfuntion(int, ReadArea, 1, uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, int WordLen, void *ptrData)
{
	unsigned long Address;
	unsigned long LongStart=Start;
	uint16_t NumElements;
	uint16_t MaxElements;
	uint16_t TotElements;
	uint16_t SizeRequested;
	uint16_t Length;

	pbyte Target;
	uintptr_t Offset = 0;
	int WordSize = 1;

	LastError=0;
	
	// If we are addressing Timers or counters the element size is 2
	if ((Area==S7AreaCT) || (Area==S7AreaTM))
		WordSize = 2;

	if (WordLen==S7WLBit) // Only one bit can be transferred in S7Protocol when WordLen==S7WLBit
		Amount=1;

    MaxElements=(PDULength-18) / WordSize; // 18 = Reply telegram header
	TotElements=Amount;
	// If we use the internal buffer only, we cannot exced the PDU limit
	if (ptrData==NULL)
	{
		if (TotElements>MaxElements)
			TotElements=MaxElements;
	}
	
    while ((TotElements>0) && (LastError==0))
    {
        NumElements=TotElements;
        if (NumElements>MaxElements)
           NumElements=MaxElements;
		
		SizeRequested =NumElements * WordSize;

		Target=(pbyte)(ptrData)+Offset;

		// Setup the telegram
		memcpy(&PDU.H, S7_RW, Size_RD); 

		// Set DB Number
		PDU.H[27] = Area;
		if (Area==S7AreaDB) 
		{
			PDU.H[25] = DBNumber>>8;
			PDU.H[26] = DBNumber & 0x00FF;
		}

		// Adjusts Start and word length
		if ((WordLen == S7WLBit) || (Area==S7AreaCT) || (Area==S7AreaTM))
		{
			Address = LongStart;
			if (WordLen==S7WLBit)
				PDU.H[22]=S7WLBit;
			else {
				if (Area==S7AreaCT)
					PDU.H[22]=S7WLCounter;
				else
					PDU.H[22]=S7WLTimer;
			}
		}
		else
			Address = LongStart<<3;

		// Num elements
		PDU.H[23]=NumElements<<8;
		PDU.H[24]=NumElements;

		// Address into the PLC
        PDU.H[30] = Address & 0x000000FF;
        Address = Address >> 8;
		PDU.H[29] = Address & 0x000000FF;
        Address = Address >> 8;
        PDU.H[28] = Address & 0x000000FF;

		if (TCPClient.tcpwrite(sn, &PDU.H[0], Size_RD)==Size_RD)
		{
			RecvISOPacket(sn, &Length);
			if (LastError==0)
			{
				if (Length>=18)
				{
					if ((Length-18==SizeRequested) && (PDU.H[31]==0xFF))
					{
						if (ptrData!=NULL)
							memcpy(Target, &PDU.DATA[0], SizeRequested); // Copies in the user's buffer
						Offset+=SizeRequested;
					}
					else
						LastError = errS7DataRead;
				}
				else
					LastError = errS7InvalidPDU;
			}
		}
		else
			LastError = errTCPDataSend;
		
		TotElements -= NumElements;
        LongStart += NumElements*WordSize;
	}
	return LastError;    
}

registerfuntion(int, ReadArea, , uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, void *ptrData)
{
    return ReadArea1(sn, Area, DBNumber, Start, Amount, S7WLByte, ptrData);
}

registerfuntion(int, ReadBit, , uint8_t sn, int Area, uint16_t DBNumber, uint16_t BitStart, bit *Bit)
{
	return ReadArea1(sn, Area, DBNumber, BitStart, 1, S7WLBit, &Bit);
}

registerfuntion(int, WriteArea, 1, uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, int WordLen, void *ptrData)
{
	unsigned long Address;
	unsigned long LongStart=Start;
	uint16_t NumElements;
	uint16_t MaxElements;
	uint16_t TotElements;
	uint16_t DataSize;
	uint16_t IsoSize;
	uint16_t Length;

	pbyte Source;
	uintptr_t Offset = 0;
	int WordSize = 1;

	LastError=0;
	
	// If we are addressing Timers or counters the element size is 2
	if ((Area==S7AreaCT) || (Area==S7AreaTM))
		WordSize = 2;

	if (WordLen==S7WLBit) // Only one bit can be transferred in S7Protocol when WordLen==S7WLBit
		Amount=1;

    MaxElements=(PDULength-35) / WordSize; // 35 = Write telegram header
	TotElements=Amount;
	if (ptrData==NULL)
	{
		if (TotElements>MaxElements)
			TotElements=MaxElements;
	}
	
    while ((TotElements>0) && (LastError==0))
    {
        NumElements=TotElements;
        if (NumElements>MaxElements)
           NumElements=MaxElements;
		// If we use the internal buffer only, we cannot exced the PDU limit
		DataSize=NumElements*WordSize;
		IsoSize=Size_WR+DataSize;

		// Setup the telegram
		memcpy(&PDU.H, S7_RW, Size_WR); 
		// Whole telegram Size
		PDU.H[2]=IsoSize>>8;
		PDU.H[3]=IsoSize & 0x00FF;
		// Data Length
		Length=DataSize+4;
		PDU.H[15]=Length>>8;
		PDU.H[16]=Length & 0x00FF;
		// Function
		PDU.H[17]=0x05;
		// Set DB Number
		PDU.H[27] = Area;
		if (Area==S7AreaDB) 
		{
			PDU.H[25] = DBNumber>>8;
			PDU.H[26] = DBNumber & 0x00FF;
		}
		// Adjusts Start and word length
		if ((WordLen == S7WLBit) || (Area==S7AreaCT) || (Area==S7AreaTM))
		{
			Address = LongStart;
			Length = DataSize;
			if (WordLen==S7WLBit)
				PDU.H[22]=S7WLBit;
			else {
				if (Area==S7AreaCT)
					PDU.H[22]=S7WLCounter;
				else
					PDU.H[22]=S7WLTimer;
			}
		}
		else
		{
			Address = LongStart<<3;
			Length = DataSize<<3;
		}
		// Num elements
		PDU.H[23]=NumElements<<8;
		PDU.H[24]=NumElements;
		// Address into the PLC
        PDU.H[30] = Address & 0x000000FF;
        Address = Address >> 8;
		PDU.H[29] = Address & 0x000000FF;
        Address = Address >> 8;
        PDU.H[28] = Address & 0x000000FF;
		
		// Transport Size
		switch (WordLen)
		{
			case S7WLBit:
				PDU.H[32] = TS_ResBit;
				break;
			case S7WLCounter:
			case S7WLTimer:
				PDU.H[32] = TS_ResOctet;
				break;
			default:
				PDU.H[32] = TS_ResByte; // byte/word/dword etc.
				break;
		};
		
		// Length
		PDU.H[33]=Length>>8;
		PDU.H[34]=Length & 0x00FF;
		// Copy data
		Source=(pbyte)(ptrData)+Offset;
		if (ptrData!=NULL)
			memcpy(&PDU.RAW[35], Source, DataSize);

		if (TCPClient.tcpwrite(sn, &PDU.H[0], IsoSize)==IsoSize)
		{
			RecvISOPacket(sn, &Length);
			if (LastError==0)
			{
				if (Length==15)
				{
					if ((PDU.H[27]!=0x00) || (PDU.H[28]!=0x00) || (PDU.H[31]!=0xFF))
						LastError = errS7DataWrite;
				}
				else
					LastError = errS7InvalidPDU;
			}
		}
		else
			LastError = errTCPDataSend;

		Offset+=DataSize;
		TotElements -= NumElements;
        LongStart += NumElements*WordSize;
	}
	return LastError;    
}

registerfuntion(int, WriteArea, , uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, void *ptrData)
{
	return WriteArea1(sn, Area, DBNumber, Start, Amount, S7WLByte, ptrData);
}

registerfuntion(int, WriteBit, 1, uint8_t sn, int Area, uint16_t DBNumber, uint16_t BitIndex, bit Bit)
{
	bit BitToWrite=Bit;
	return WriteArea1(sn, Area, DBNumber, BitIndex, 1, S7WLBit, &BitToWrite);
}

registerfuntion(int, WriteBit, , uint8_t sn, int Area, uint16_t DBNumber, uint16_t ByteIndex, uint16_t BitInByte, bit Bit)
{
	bit BitToWrite=Bit;
	return  WriteArea1(sn, Area, DBNumber, ByteIndex*8+BitInByte, 1, S7WLBit, &BitToWrite);
}

registerfuntion(int, GetPDULength, , void)
{ return PDULength; }

//-----------------------------------------------------------------------------
#ifdef _EXTENDED

registerfuntion(int, GetDBSize, , uint8_t sn, uint16_t DBNumber, uint16_t *Size)
{
	uint16_t Length;
	LastError=0;
	*Size=0;
	// Setup the telegram
	memcpy(&PDU.H[0], S7_BI, sizeof(S7_BI));
	// Set DB Number
    PDU.RAW[31]=(DBNumber / 10000)+0x30;
    DBNumber=DBNumber % 10000;
    PDU.RAW[32]=(DBNumber / 1000)+0x30;
    DBNumber=DBNumber % 1000;
    PDU.RAW[33]=(DBNumber / 100)+0x30;
    DBNumber=DBNumber % 100;
    PDU.RAW[34]=(DBNumber / 10)+0x30;
    DBNumber=DBNumber % 10;
    PDU.RAW[35]=(DBNumber / 1)+0x30;
	if (TCPClient.tcpwrite(sn, &PDU.H[0], sizeof(S7_BI))==sizeof(S7_BI))
	{
		RecvISOPacket(sn, &Length);
		if (LastError==0)
		{
			if (Length>25) // 26 is the minimum expected
			{
				if ((PDU.RAW[37]==0x00) && (PDU.RAW[38]==0x00) && (PDU.RAW[39]==0xFF))
				{
					*Size=PDU.RAW[83];
					*Size=(*Size<<8)+PDU.RAW[84];
				}
				else
					LastError = errS7Function;
			}
			else
				LastError = errS7InvalidPDU;
		}
	}
	else
		LastError = errTCPDataSend;
	
	return LastError;    
}

registerfuntion(int, DBGet, ,  uint8_t sn, uint16_t DBNumber, void *ptrData, uint16_t *Size)
{
	uint16_t Length;
	int Result;

	Result=GetDBSize(sn, DBNumber, &Length);
	if (Result==0)
	{
		if (Length<=*Size) // Check if the buffer supplied is big enough 
		{
			Result=ReadArea(sn ,S7AreaDB, DBNumber, 0, Length, ptrData);
			if (Result==0)
				*Size=Length;
		}
		else
			Result=errBufferTooSmall;
	}

	return Result;
}

registerfuntion(int, PlcStop, , uint8_t sn)
{
	uint16_t Length;
	LastError=0;
	// Setup the telegram
	memcpy(&PDU.H, S7_STOP, sizeof(S7_STOP)); 
	if (TCPClient.tcpwrite(sn, &PDU.H[0], sizeof(S7_STOP))==sizeof(S7_STOP))
	{
		RecvISOPacket(sn ,&Length);
		if (LastError==0)
		{
			if (Length>12) // 13 is the minimum expected
			{
				if ((PDU.H[27]!=0x00) || (PDU.H[28]!=0x00))
					LastError = errS7Function;
			}
			else
				LastError = errS7InvalidPDU;
		}
	}
	else
		LastError = errTCPDataSend;
	
	return LastError;
}

registerfuntion(int, PlcStart, ,  uint8_t sn)
{
	uint16_t Length;
	LastError=0;
	// Setup the telegram
	memcpy(&PDU.H, S7_START, sizeof(S7_START)); 
	if (TCPClient.tcpwrite(sn, &PDU.H[0], sizeof(S7_START))==sizeof(S7_START))
	{
		RecvISOPacket(sn, &Length);
		if (LastError==0)
		{
			if (Length>12) // 13 is the minimum expected
			{
				if ((PDU.H[27]!=0x00) || (PDU.H[28]!=0x00))
					LastError = errS7Function;
			}
			else
				LastError = errS7InvalidPDU;
		}
	}
	else
		LastError = errTCPDataSend;
	
	return LastError;    
}

registerfuntion(int, GetPlcStatus, , uint8_t sn, int *Status)
{
	uint16_t Length;
	LastError=0;
	// Setup the telegram
	memcpy(&PDU.H, S7_PLCGETS, sizeof(S7_PLCGETS)); 
	if (TCPClient.tcpwrite(sn, &PDU.H[0], sizeof(S7_PLCGETS))==sizeof(S7_PLCGETS))
	{
		RecvISOPacket(sn, &Length);
		if (LastError==0)
		{
			if (Length>53) // 54 is the minimum expected
			{
				switch (PDU.RAW[54])
				{
					case S7CpuStatusUnknown :
					case S7CpuStatusRun     :
					case S7CpuStatusStop    : *Status=PDU.RAW[54];
					break;
					default :
					// Since RUN status is always 0x08 for all CPUs and CPs, STOP status
					// sometime can be coded as 0x03 (especially for old cpu...)
						*Status=S7CpuStatusStop;
				}
			}
			else
				LastError = errS7InvalidPDU;
		}
	}
	else
		LastError = errTCPDataSend;
	
	return LastError;    
}

registerfuntion(int, IsoExchangeBuffer, , uint8_t sn, uint16_t *Size)
{
	LastError=0;

	if (TCPClient.tcpwrite(sn, &PDU.H[0], (int)(Size))==*Size)
		RecvISOPacket(sn, Size);
	else
		LastError = errTCPDataSend;
	
	return LastError;
}
#endif // _EXTENDED
//-----------------------------------------------------------------------------

ucS7client ucS7Client = {
    .SetConnectionParams = SetConnectionParams,
    .SetConnectionType = SetConnectionType,
    .ConnectTo = ConnectTo,
    .Connect = Connect,
    .Disconnect = Disconnect,
    .ReadArea1 = ReadArea1,
    .ReadArea = ReadArea,
    .ReadBit = ReadBit,
    .WriteArea1 = WriteArea1,
    .WriteArea = WriteArea,
    .WriteBit1 = WriteBit1, 
    .WriteBit = WriteBit,
    .GetPDULength = GetPDULength,
#ifdef _EXTENDED
    .GetDBSize = GetDBSize,
    .DBGet = DBGet,
    .PlcStart = PlcStart,
    .PlcStop = PlcStop,
    .GetPlcStatus = GetPlcStatus,
    .IsoExchangeBuffer = IsoExchangeBuffer,
#endif // _EXTENDED
};

