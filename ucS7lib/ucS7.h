#ifndef _UCS7_H
#define _UCS7_H

#include <stdint.h>
#include "TCPClient.h"

typedef uint8_t byte;
typedef uint16_t word;          // 16 bit unsigned integer

typedef int16_t integer;        // 16 bit signed integer
typedef unsigned long dword;    // 32 bit unsigned integer
typedef long dint;              // 32 bit signed integer

typedef byte *pbyte;
typedef word *pword;
typedef dword *pdword;
typedef integer *pinteger;
typedef dint *pdint;
typedef float *pfloat;
typedef char *pchar;

typedef enum{Ok,Error}Status;
typedef enum{False,True}bit;

typedef uint8_t IPAddress;

// Memory moduels

// #define _SMALL
// #define _NORMAL
#define _EXTENDED

#if defined(_NORMAL) || defined(_EXTENDED)
# define _ucS7HELPER
#endif
// Error Codes 
// from 0x0001 up to 0x00FF are severe errors, the Client should be disconnected
// from 0x0100 are S7 Errors such as DB not found or address beyond the limit etc..
// For Arduino Due the error code is a 32 bit integer but this doesn't change the constants use.
#define errTCPConnectionFailed 0x0001
#define errTCPConnectionReset  0x0002
#define errTCPDataRecvTout     0x0003
#define errTCPDataSend         0x0004
#define errTCPDataRecv         0x0005
#define errISOConnectionFailed 0x0006
#define errISONegotiatingPDU   0x0007
#define errISOInvalidPDU       0x0008

#define errS7InvalidPDU        0x0100
#define errS7SendingPDU        0x0200
#define errS7DataRead          0x0300
#define errS7DataWrite         0x0400
#define errS7Function          0x0500

#define errBufferTooSmall      0x0600

// Connection Type
#define PG       0x01
#define OP       0x02
#define S7_Basic 0x03

// ISO and PDU related constants
#define ISOSize        7  // Size of TPKT + COTP Header
#define isotcp       102  // ISOTCP Port
#define MinPduSize    16  // Minimum S7 valid telegram size
#define MaxPduSize   247  // Maximum S7 valid telegram size (we negotiate 240 bytes + ISOSize)
#define CC          0xD0  // Connection confirm
#define Shift         17  // We receive data 17 bytes above to align with PDU.DATA[]

// S7 ID Area (Area that we want to read/write)
#define S7AreaPE    0x81
#define S7AreaPA    0x82
#define S7AreaMK    0x83
#define S7AreaDB    0x84
#define S7AreaCT    0x1C
#define S7AreaTM    0x1D

// WordLength
#define S7WLBit     0x01
#define S7WLByte    0x02
#define S7WLWord    0x04
#define S7WLDWord   0x06
#define S7WLReal    0x08
#define S7WLCounter 0x1C
#define S7WLTimer   0x1D

#define TS_ResBit   0x03
#define TS_ResByte  0x04
#define TS_ResInt   0x05
#define TS_ResReal  0x07
#define TS_ResOctet 0x09


#define RxOffset    18
#define Size_RD     31
#define Size_WR     35


typedef union{
	struct {
		byte H[Size_WR];                      // PDU Header
		byte DATA[MaxPduSize-Size_WR+Shift];  // PDU Data
	};
	byte RAW[MaxPduSize+Shift];
}__attribute__((packed)) TPDU;

#ifdef _ucS7HELPER

typedef struct _ucS7Helper {
	Status (*BitAt1)(void *Buffer, int ByteIndex, byte BitIndex);
	Status (*BitAt2)(int ByteIndex, int BitIndex);
	byte   (*ByteAt1)(void *Buffer, int index);
	byte   (*ByteAt2)(int index);
	word   (*WordAt1)(void *Buffer, int index);
	word   (*WordAt2)(int index);
	dword  (*DWordAt1)(void *Buffer, int index);
	dword  (*DWordAt2)(int index);
	float  (*FloatAt1)(void *Buffer, int index);
	float  (*FloatAt2)(int index);
	integer   (*IntegerAt1)(void *Buffer, int index);
	integer   (*IntegerAt2)(int index);
	long   (*DintAt1)(void *Buffer, int index);
	long   (*DintAt2)(int index);
	void   (*SetBitAt1)(void *Buffer, int ByteIndex, int BitIndex, bit Value);
	void   (*SetBitAt2)(int ByteIndex, int BitIndex, bit Value);
	void   (*SetByteAt1)(void *Buffer, int index, byte value);
	void   (*SetByteAt2)(int index, byte value);
	void   (*SetIntAt1)(void *Buffer, int index, integer value);
	void   (*SetIntAt2)(int index, integer value);
	void   (*SetDIntAt1)(void *Buffer, int index, dint value);
	void   (*SetDIntAt2)(int index, dint value);
	void   (*SetWordAt1)(void *Buffer, int index, word value);
	void   (*SetWordAt2)(int index, word value);
	void   (*SetDWordAt1)(void *Buffer, int index, dword value);
	void   (*SetDWordAt2)(int index, word value);
	void   (*SetFloatAt1)(void *Buffer, int index, float value);
	void   (*SetFloatAt2)(int index, float value);
	char * (*StringAt1)(void *Buffer, int index);
	char * (*StringAt2)(int index);
	void   (*SetStringAt1)(void *Buffer, int index, char *value);
	void   (*SetStringAt2)(int index, char *value);
}ucS7Helper;

extern ucS7Helper ucS7;

#endif // _ucS7HELPER


extern bit Connected;
extern int LastError;
extern uint16_t RecvTimeout;

typedef struct _ucS7client {
	void (*SetConnectionParams)(IPAddress* Address, uint16_t LocalTSAP, uint16_t RemoteTSAP);
	void (*SetConnectionType)(uint16_t ConnectionType);
	int (*ConnectTo)(uint8_t sn, IPAddress* Address, uint16_t Rack, uint16_t Slot);
	int (*Connect)(uint8_t sn);
	void (*Disconnect)(uint8_t sn);
	int (*ReadArea1)(uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, int WordLen, void *ptrData); 
	int (*ReadArea)(uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, void *ptrData); 
	int (*ReadBit)(uint8_t sn, int Area, uint16_t DBNumber, uint16_t BitStart, bit *Bit); 
	int (*WriteArea1)(uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, int WordLen, void *ptrData); 
	int (*WriteArea)(uint8_t sn, int Area, uint16_t DBNumber, uint16_t Start, uint16_t Amount, void *ptrData); 
	int (*WriteBit1)(uint8_t sn, int Area, uint16_t DBNumber, uint16_t BitIndex, bit Bit); 
	int (*WriteBit)(uint8_t sn, int Area, uint16_t DBNumber, uint16_t ByteIndex, uint16_t BitInByte, bit Bit); 
    int (*GetPDULength)(void);
#ifdef _EXTENDED
	int (*GetDBSize)(uint8_t sn, uint16_t DBNumber, uint16_t *Size);
	int (*DBGet)(uint8_t sn, uint16_t DBNumber, void *ptrData, uint16_t *Size);
    int (*PlcStart)(uint8_t sn); // Warm start
    int (*PlcStop)(uint8_t sn);
    int (*GetPlcStatus)(uint8_t sn, int *Status);
	int (*IsoExchangeBuffer)(uint8_t sn, uint16_t *Size);
#endif // _EXTENDED

}ucS7client;

extern ucS7client ucS7Client;

#endif // _UCS7_H